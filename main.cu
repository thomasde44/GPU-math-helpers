#include "functions.cuh"
#include <time.h>  


int main(void)
{
    srand (time(NULL));
    // initialize host array
    float x[32];
    float y[32];
    for (int i = 0; i < 32; i++){
        x[i] = i;
        y[i] = i;//rand() % 50;
    }
    float z[32];
    float w[32];
    // transfer to device
    thrust::device_vector<float> d_x(x, x + 32);
    thrust::device_vector<float> vec_b(y, y + 32);
    thrust::device_vector<float> vec_c(z, z + 32);
    thrust::device_vector<float> vec_d(w, w + 32);
    thrust::device_vector<float> vec_a(w, w + 32);
    
    // setup arguments
    // square<float>        unary_op;
    // thrust::plus<float> binary_op;
    // float init = 0;

    // compute norm
    // float norm = std::sqrt( thrust::transform_reduce(d_x.begin(), d_x.end(), unary_op, init) );
    float a = standard_deviation<float>(d_x);
    float b = standard_deviation<float>(vec_b);
    // float c = correlation_coeff<float>(a, b, d_x, vec_b, vec_c, vec_d);
    std::cout << a << std::endl;
    std::cout << b << std::endl;
    // std::cout << c << std::endl;
    int n = 32;
    thrust::device_vector<float> val_in(x, x+32);
    thrust::device_vector<float> val_out(n);
    // thrust::copy(val_in.begin() + 1, val_in.end(), val_out.begin());
    // val_out[n-1] = val_in[0];
    // // add the new value to the last position
    // for (int i = 0; i < val_out.size(); i++){
    //     std::cout << val_out[i] << std::endl;
    // }
    thrust::device_vector<float> res(w, w + 32);
    // thrust::device_vector<float> cov(n);
    res = correlogram<float>(a, a, d_x, d_x);
    // std::cout << cov << std::endl;
    for (int i = 0; i < res.size(); i++) {
        std::cout << res[i] << " ";
    }
 
    std::cout << std::endl;

    fft_1d<float>(d_x);
    // for (int i = 0; i < res.size(); i++) {
    //     std::cout << d_x[i] << " ";
    // }

    return 0;
}