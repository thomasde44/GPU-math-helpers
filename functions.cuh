#include <thrust/transform_reduce.h>
#include <thrust/functional.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/device_malloc.h>
#include <thrust/device_free.h>

#include <cmath>
#include <iostream>
#include <cuda_runtime.h>
#include <cufft.h>
#include <cufftXt.h>
#include "common/inc/helper_cuda.h"
#include "common/inc/helper_functions.h"

template <typename T> T mean_vector(thrust::device_vector<T> vec) {
    return (thrust::reduce(vec.begin(), vec.end(), (T) 0, thrust::plus<T>())) / vec.size();
}



template <typename T> T variance(thrust::device_vector<T> vec_a) {
    
    thrust::device_vector<T> vec_b(vec_a.size());
    thrust::device_vector<T> vec_c(vec_a.size());

    T mean = (thrust::reduce(vec_a.begin(), vec_a.end(), (T) 0, thrust::plus<T>())) / vec_a.size();
    thrust::fill(vec_b.begin(), vec_b.end(), mean);
    thrust::transform(vec_a.begin(), vec_a.end(), vec_b.begin(), vec_c.begin(), thrust::minus<T>());
    thrust::transform(vec_c.begin(), vec_c.end(), vec_a.begin(), thrust::square<T>());

    return thrust::reduce(vec_a.begin(), vec_a.end(), (T) 0, thrust::plus<T>()) / vec_a.size();

}

template <typename T> T standard_deviation(thrust::device_vector<T> vec_a) {
    
    thrust::device_vector<T> vec_b(vec_a.size());
    thrust::device_vector<T> vec_c(vec_a.size());

    T mean = (thrust::reduce(vec_a.begin(), vec_a.end(), (T) 0, thrust::plus<T>())) / vec_a.size();
    thrust::fill(vec_b.begin(), vec_b.end(), mean);
    thrust::transform(vec_a.begin(), vec_a.end(), vec_b.begin(), vec_c.begin(), thrust::minus<T>());
    thrust::transform(vec_c.begin(), vec_c.end(), vec_a.begin(), thrust::square<T>());
    
    return std::sqrt(thrust::reduce(vec_a.begin(), vec_a.end(), (T) 0, thrust::plus<T>()));

}

template <typename T> T covariance(thrust::device_vector<T> vec_a,
                                    thrust::device_vector<T> vec_b
                                    ) {
    T mean_a = (thrust::reduce(vec_a.begin(), vec_a.end(), (T) 0, thrust::plus<T>())) / vec_a.size();
    T mean_b = (thrust::reduce(vec_b.begin(), vec_b.end(), (T) 0, thrust::plus<T>())) / vec_b.size();
    thrust::device_vector<T> vec_container_1(vec_a.size());
    thrust::device_vector<T> vec_container_2(vec_a.size());
    thrust::fill(vec_container_1.begin(), vec_container_1.end(), mean_a);
    thrust::transform(vec_a.begin(), vec_a.end(), vec_container_1.begin(), vec_container_2.begin(), thrust::minus<T>());

    thrust::fill(vec_container_1.begin(), vec_container_1.end(), mean_b);
    thrust::transform(vec_b.begin(), vec_b.end(), vec_container_1.begin(), vec_a.begin(), thrust::minus<T>()); 

    thrust::transform(vec_a.begin(), vec_a.end(), vec_container_2.begin(), vec_container_1.begin(), thrust::multiplies<T>());
    T tmp = thrust::reduce(vec_container_1.begin(), vec_container_1.end(), (T) 0, thrust::plus<T>());
    return tmp / (vec_a.size() - 1);

}

// vec_a correlated with vec_b, vec_c and vec_d are used as accumulators 
template <typename T> T correlation_coeff(T std1, T std2, 
                                        thrust::device_vector<T> vec_a, 
                                        thrust::device_vector<T> vec_b
                                        ) {
    
    thrust::device_vector<T> vec_c(vec_a.size()); 
    thrust::device_vector<T> vec_d(vec_a.size());
    T mean = (thrust::reduce(vec_a.begin(), vec_a.end(), (T) 0, thrust::plus<T>())) / vec_a.size();
    thrust::fill(vec_c.begin(), vec_c.end(), mean);
    thrust::transform(vec_a.begin(), vec_a.end(), vec_c.begin(), vec_d.begin(), thrust::minus<T>());

    mean = (thrust::reduce(vec_b.begin(), vec_b.end(), (T) 0, thrust::plus<T>())) / vec_b.size();
    thrust::fill(vec_c.begin(), vec_c.end(), mean);
    thrust::transform(vec_b.begin(), vec_b.end(), vec_c.begin(), vec_a.begin(), thrust::minus<T>());

    thrust::transform(vec_a.begin(), vec_a.end(), vec_d.begin(), vec_c.begin(), thrust::multiplies<T>());
    T tmp = thrust::reduce(vec_c.begin(), vec_c.end(), (T) 0, thrust::plus<T>());
    return tmp / (std1*std2);

}

// cross correlation of a and b. Coefficients for all lag 1 .. vec.size() stored in result
template <typename T> thrust::device_vector<T> correlogram(T std1,T std2,
                                                thrust::device_vector<T> vec_a, 
                                                thrust::device_vector<T> vec_b
                                                ) {
    thrust::device_vector<T> shift_a(vec_a.size());
    thrust::device_vector<T> shift_b(vec_a.size());
    thrust::device_vector<T> vec_c(vec_a.size());
    thrust::device_vector<T> vec_d(vec_a.size());
    thrust::device_vector<T> res(vec_a.size());
    thrust::copy(vec_a.begin(), vec_a.end(), shift_a.begin());

    T mean_a = (thrust::reduce(vec_a.begin(), vec_a.end(), (T) 0, thrust::plus<T>())) / vec_a.size();
    thrust::fill(vec_c.begin(), vec_c.end(), mean_a);
    thrust::transform(vec_a.begin(), vec_a.end(), vec_c.begin(), vec_d.begin(), thrust::minus<T>());

    T mean_b = (thrust::reduce(vec_b.begin(), vec_b.end(), (T) 0, thrust::plus<T>())) / vec_b.size();
    thrust::fill(vec_c.begin(), vec_c.end(), mean_b);
    thrust::transform(vec_b.begin(), vec_b.end(), vec_c.begin(), vec_a.begin(), thrust::minus<T>());

    thrust::transform(vec_a.begin(), vec_a.end(), vec_d.begin(), vec_c.begin(), thrust::multiplies<T>());
    T Co = thrust::reduce(vec_c.begin(), vec_c.end(), (T) 0, thrust::plus<T>());

    for (int i = 0; i < shift_a.size(); i++) {

        thrust::copy(shift_a.begin() + 1, shift_a.end(), shift_b.begin());
        shift_b[vec_a.size()-1] = shift_a[0];
        thrust::copy(shift_b.begin(), shift_b.end(), shift_a.begin());
        

        // 
        thrust::copy(shift_a.begin(), shift_a.end(), vec_a.begin());
        T mean = (thrust::reduce(vec_a.begin(), vec_a.end(), (T) 0, thrust::plus<T>())) / vec_a.size();
        thrust::fill(vec_c.begin(), vec_c.end(), mean_a);
        thrust::transform(vec_a.begin(), vec_a.end(), vec_c.begin(), vec_d.begin(), thrust::minus<T>());

        mean = (thrust::reduce(vec_b.begin(), vec_b.end(), (T) 0, thrust::plus<T>())) / vec_b.size();
        thrust::fill(vec_c.begin(), vec_c.end(), mean_b);
        thrust::transform(vec_b.begin(), vec_b.end(), vec_c.begin(), vec_a.begin(), thrust::minus<T>());

        thrust::transform(vec_a.begin(), vec_a.end(), vec_d.begin(), vec_c.begin(), thrust::multiplies<T>());
        
        //
        T tmp = thrust::reduce(vec_c.begin(), vec_c.end(), (T) 0, thrust::plus<T>());

        res[i] = tmp / Co ;

    }
    return res;
    
}

// take fft of a sequence using cufft
template <typename T> void fft_1d(thrust::device_vector<T> vec_a) {
    for (int i = 0; i < vec_a.size(); i++) {
        std::cout << vec_a[i] << std::endl;
    }
    static cufftHandle plan;
    size_t a = vec_a.size();
    // T * raw_ptr = thrust::raw_pointer_cast(vec_a.data());
    // // cudaMemset(raw_ptr, 0, a * sizeof(T));
    // float2 * thrust_ptr = (float2*)raw_ptr;
    float2 * res_ptr;
    float2 ptr[a];
    // checkCudaErrors(cudaMemcpy(ptr, h_ptr, a*sizeof(float2), cudaMemcpyDeviceToHost));
    // std::cout << ptr[0].x << std::endl;
    // T * raw_ptr = thrust::raw_pointer_cast(dev_ptr);
    // int a = vec_a.size();
    // float2 h_ptr[a];
    // float2 * d_ptr;
    #pragma unroll
    for (int i = 0; i < vec_a.size(); i++) {
        ptr[i].x = vec_a[i];
        ptr[i].y = 0;
    }
    checkCudaErrors(cudaMalloc(reinterpret_cast<void **>(&res_ptr), a*sizeof(float2)));
    checkCudaErrors(cudaMemcpy(res_ptr, ptr, a*sizeof(float2), cudaMemcpyHostToDevice));
    // batch size is the numbers of vectors, vector size is the length of a vector. Say you had an NxN 
    // circulant matrix you would have a batch size of N. Say you have a vector length of 15 and you wanted to
    // process them in a processing size of 1024x256 you would have a batch size of 17476.
    checkCudaErrors(cufftPlan1d(&plan, a, CUFFT_C2C, 1));
    checkCudaErrors(cufftExecC2C(plan, reinterpret_cast<float2*>(res_ptr), reinterpret_cast<float2*>(res_ptr), CUFFT_FORWARD));

    checkCudaErrors(cudaMemcpy(ptr, res_ptr, a*sizeof(float2), cudaMemcpyDeviceToHost));
    for (int i = 0; i < vec_a.size(); i++) {
        std::cout << ptr[i].x << " " << ptr[i].y << std::endl;
    }



}